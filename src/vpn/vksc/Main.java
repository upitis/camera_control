package vpn.vksc;

import vpn.vksc.controller.CameraController;
import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.packets.ViscaPacket;
import vpn.vksc.controller.network.packets.iquiry.InquiryNetworkReplyPacket;
import vpn.vksc.model.Camera;

import java.io.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) throws IOException {
        printHelp();
        CameraController cameraController = CameraController.getInstance();
//        cameraController.setIpAddress("192.168.110.19"); //temp for testing0
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String s = br.readLine();
            s = s.trim();
            if (s.matches("exit")) {
                break;
            }
            if (s.matches("^ip .+")) {
                Pattern p = Pattern.compile("ip\\s+(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})");
                Matcher m = p.matcher(s);
                if (m.find()) {
                    cameraController.setIpAddress(m.group(1));
                }
            }
            if (s.matches("^ch \\d+")) {
                Pattern p = Pattern.compile("ch\\s+(\\d+)");
                Matcher m = p.matcher(s);
                if (m.find()) {
                    Integer camNumber = Integer.parseInt(m.group(1)) - 1;
                    try {
                        List<ViscaPacket> packetL = cameraController.getViscaCamerasInNetwork().get();
                        if (camNumber < packetL.size() &&
                                packetL.get(camNumber) instanceof InquiryNetworkReplyPacket) {
                            String ip = ((InquiryNetworkReplyPacket) packetL.get(camNumber)).getIpAddress();
                            cameraController.setIpAddress(ip);
                            System.out.println("Controller reset to ip: " + ip );
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (s.matches("\\d{1,2}")) {
                cameraController.recallPreset(Integer.parseInt(s)-1);
            }
            if (s.matches("^savepos \\d{1,2}")) {
                Pattern p = Pattern.compile("savepos (\\d{1,2})");
                Matcher m = p.matcher(s);
                if (m.find()) {
                    cameraController.savePreset(Integer.parseInt(m.group(1))-1);
                }
            }

            if (s.matches("find")) {
                int i=0;
                for (Camera cam : cameraController.findSonyCameraInBroadcastDomain()) {
                    i++;
                    System.out.println("" + i + " " + cam);
                };
            }
            if (s.matches("^change ip.+")) {
                String mac = null;
                String ip = null;
                String mask = null;
                String name = null;
                Matcher matcher;
                matcher = Pattern.compile("macAddress='(.+?)'").matcher(s);
                if (matcher.find()) { mac = matcher.group(1); }
                matcher = Pattern.compile("ipAddress='(.+?)'").matcher(s);
                if (matcher.find()) { ip = matcher.group(1); }
                matcher = Pattern.compile("netmask='(.+?)'").matcher(s);
                if (matcher.find()) { mask = matcher.group(1); }
                matcher = Pattern.compile("name='(.+?)'").matcher(s);
                if (matcher.find()) { name = matcher.group(1); }
                if ( mac!= null && ip!=null && mask!=null && name!=null) {
                    cameraController.changeCameraIpAddress(mac, ip, mask, name);
                }
            }
            if (s.matches("^on$")){
                cameraController.powerOn();
            }
            if (s.matches("^off$")){
                cameraController.powerOff();
            }

            if (s.matches("^osd on")){
                cameraController.osdOn();
            }
            if (s.matches("^osd off")){
                cameraController.osdOff();
            }

            if (s.matches("^help")){
                printHelp();
            }
            if (s.matches("^set pos.+")){
                int pan;
                int tilt;
                Matcher matcher = Pattern.compile("[(](-{0,1}[0-9]+?,-{0,1}[0-9]+?)[)]").matcher(s);
                if (matcher.find()) {
                    pan = Integer.parseInt(matcher.group(1).split(",")[0]);
                    tilt = Integer.parseInt(matcher.group(1).split(",")[1]);
                    cameraController.setAbsPos((short)pan, (short)tilt);
                }
            }
            if (s.matches("^set zoom.+")){
                int zoom;
                Matcher matcher = Pattern.compile("([0-9]{1,5})").matcher(s);
                if (matcher.find()) {
                    zoom = Integer.parseInt(matcher.group(1));
                    cameraController.setZoomPos((short)zoom);
                }
            }

            if (s.matches("^up$")){
                cameraController.up(1, 0);
            }
            if (s.matches("^down$")){
                cameraController.down(1,0);
            }
            if (s.matches("^left$")){
                cameraController.left(1, 0);
            }
            if (s.matches("^right$")){
                cameraController.right(1, 0);
            }
            if (s.matches("^[+]$")){
                cameraController.zoomIn(1,0);
            }
            if (s.matches("^-$")){
                cameraController.zoomOut(1,0);
            }
            if (s.matches("^up .+")) {
                cameraController.up(getCamSpeed(s), getDelay(s));
            }
            if (s.matches("^down .+")) {
                cameraController.down(getCamSpeed(s),getDelay(s));
            }
            if (s.matches("^left .+")) {
                cameraController.left(getCamSpeed(s),getDelay(s));
            }
            if (s.matches("^right .+")) {
                cameraController.right(getCamSpeed(s),getDelay(s));
            }
            if (s.matches("^[+] .+")) {
                cameraController.zoomIn(getCamSpeed(s),getDelay(s));
            }
            if (s.matches("^- .+")) {
                cameraController.zoomOut(getCamSpeed(s),getDelay(s));
            }


        }
        System.exit(0);
    }
    
    private static int getCamSpeed(String s){
        Matcher m;
        if ((m = Pattern.compile("(\\d{1,2})").matcher(s)).find()) {
            return Integer.parseInt(m.group(1));
        }
        return 1;
    }

    private static int getDelay(String s){
        Matcher m;
        if ((m = Pattern.compile("\\d+\\s+(\\d{1,4})").matcher(s)).find()) {
            return Integer.parseInt(m.group(1));
        }
        return 0;
    }
    

    private static void printHelp(){
        ClassLoader classLoader = new Main().getClass().getClassLoader();
        BufferedReader br = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream("help")));
        String s;
        try {
            while ( (s = br.readLine()) != null) {
                System.out.println(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
