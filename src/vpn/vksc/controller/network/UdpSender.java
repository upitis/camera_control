package vpn.vksc.controller.network;

import vpn.vksc.controller.network.packets.ViscaPacket;

import java.io.IOException;
import java.net.*;

public class UdpSender {
    private String remoteAddress;
    private int remotePort;
    private int usedLocalPort;

    public UdpSender(String remoteAddress, int remotePort) {
        this.remoteAddress = remoteAddress;
        this.remotePort = remotePort;
    }

    public Boolean sendData(ViscaPacket data, boolean isWithHeader){
        try {
            InetAddress inetAddress = InetAddress.getByName(remoteAddress);
            byte[] sdata;
            if (isWithHeader) {
                sdata = data.getRawData();
            } else {
                sdata = data.getPayload();
            }
            DatagramPacket packet = new DatagramPacket(sdata, sdata.length, inetAddress, remotePort);
            DatagramSocket datagramSocket = new DatagramSocket();
            datagramSocket.send(packet);
            usedLocalPort = datagramSocket.getLocalPort();
            datagramSocket.close();
            return true;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            System.out.printf("Неправильно задан адрес узла");
        } catch (SocketException se) {
            se.printStackTrace();
            System.out.printf("Не удалось создать сокет для отправки");
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("Не удалось отправить пакет");
        }
        return false;
    }


    public int getUsedLocalPort() {
        return usedLocalPort;
    }
}
