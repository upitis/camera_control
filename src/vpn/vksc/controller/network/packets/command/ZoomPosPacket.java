package vpn.vksc.controller.network.packets.command;

import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.packets.PayloadType;
import vpn.vksc.controller.network.packets.ViscaPacket;

public class ZoomPosPacket extends ViscaPacket {
    public ZoomPosPacket(int sequenceNumber, short zoomPos) {
        super(PayloadType.VISCA_COMMAND, sequenceNumber, new byte[0]);

        String zoomP = HexUtils.shortToMyHexString((byte)((zoomPos & 0xF000) >> 12)) +
                HexUtils.shortToMyHexString((byte)((zoomPos & 0x0F00) >> 8)) +
                HexUtils.shortToMyHexString((byte)((zoomPos & 0x00F0) >> 4)) +
                HexUtils.shortToMyHexString((byte)(zoomPos & 0x000F));

        this.setPayload(HexUtils.stringToByteArray("<0x81><0x01><0x04><0x47>" + zoomP +"<0xFF>"));
    }
}
