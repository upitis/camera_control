package vpn.vksc.controller.network.packets.command;

import vpn.vksc.controller.network.packets.ViscaPacket;

public class AckPacket extends ViscaPacket {

    public AckPacket(byte[] rawData) {
        super(rawData);
    }

    @Override
    public String toString() {
        return  "Command with sequence number <" + getSequenceNumber() + "> received by camera";
    }
}
