package vpn.vksc.controller.network.packets.command;

import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.packets.PayloadType;
import vpn.vksc.controller.network.packets.ViscaPacket;

public class AbsolutePositionPacket extends ViscaPacket {
    public AbsolutePositionPacket(int sequenceNumber, int panSpeed, int tiltSpeed, short panPos, short tiltPos) {
        super(PayloadType.VISCA_COMMAND, sequenceNumber, new byte[0]);

        String panS = HexUtils.shortToMyHexString((byte) (panSpeed % 0x19));
        String tiltS = HexUtils.shortToMyHexString((byte) (tiltSpeed % 0x18));
        String panP = HexUtils.shortToMyHexString((byte)((panPos & 0xF000) >> 12)) +
                    HexUtils.shortToMyHexString((byte)((panPos & 0x0F00) >> 8)) +
                    HexUtils.shortToMyHexString((byte)((panPos & 0x00F0) >> 4)) +
                    HexUtils.shortToMyHexString((byte)(panPos & 0x000F));
        String tiltP = HexUtils.shortToMyHexString((byte)((tiltPos & 0xF000) >> 12)) +
                HexUtils.shortToMyHexString((byte)((tiltPos & 0x0F00) >> 8)) +
                HexUtils.shortToMyHexString((byte)((tiltPos & 0x00F0) >> 4)) +
                HexUtils.shortToMyHexString((byte)(tiltPos & 0x000F));
        this.setPayload(HexUtils.stringToByteArray("<0x81><0x01><0x06><0x02>" + panS + tiltS + panP + tiltP +"<0xFF>"));
    }
}
