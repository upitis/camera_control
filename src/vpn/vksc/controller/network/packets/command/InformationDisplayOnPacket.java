package vpn.vksc.controller.network.packets.command;

import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.packets.PayloadType;
import vpn.vksc.controller.network.packets.ViscaPacket;

public class InformationDisplayOnPacket extends ViscaPacket {
    public InformationDisplayOnPacket(int sequenceNumber) {
        super(PayloadType.VISCA_COMMAND, sequenceNumber,
                HexUtils.stringToByteArray("<0x81><0x01><0x7E><0x01><0x18><0x02><0xFF>"));
    }
}
