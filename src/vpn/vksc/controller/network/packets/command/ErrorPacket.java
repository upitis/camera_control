package vpn.vksc.controller.network.packets.command;

import vpn.vksc.controller.network.packets.ViscaPacket;

public class ErrorPacket extends ViscaPacket {
    public ErrorPacket(byte[] rawData) {
        super(rawData);
    }

    @Override
    public String toString() {
        return "Error with running command on camera";
    }
}
