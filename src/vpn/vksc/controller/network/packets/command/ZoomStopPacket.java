package vpn.vksc.controller.network.packets.command;

import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.packets.PayloadType;
import vpn.vksc.controller.network.packets.ViscaPacket;

public class ZoomStopPacket extends ViscaPacket {
    public ZoomStopPacket(int sequenceNumber) {
        super(PayloadType.VISCA_COMMAND, sequenceNumber,
                HexUtils.stringToByteArray("<0x81><0x01><0x04><0x07><0x00><0xFF>"));
    }}
