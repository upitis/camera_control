package vpn.vksc.controller.network.packets.command;

import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.packets.PayloadType;
import vpn.vksc.controller.network.packets.ViscaPacket;

public class SavePresetPacket extends ViscaPacket {
    public SavePresetPacket(int sequenceNumber, byte preset) {
        super(PayloadType.VISCA_COMMAND, sequenceNumber,
                HexUtils.stringToByteArray("<0x81><0x01><0x04><0x3F><0x01>" +
                                            HexUtils.shortToMyHexString(preset) + "<0xFF>"));
    }
}
