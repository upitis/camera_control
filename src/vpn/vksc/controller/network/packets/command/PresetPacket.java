package vpn.vksc.controller.network.packets.command;


import vpn.vksc.controller.network.packets.PayloadType;
import vpn.vksc.controller.network.packets.ViscaPacket;

public class PresetPacket extends ViscaPacket {

    public PresetPacket(byte preset, int sequenceNumber) {
        super(PayloadType.VISCA_COMMAND, sequenceNumber,
                new byte[] {(byte)0x81,(byte)0x01,(byte)0x04,(byte)0x3F,(byte)0x02, preset,(byte)0xFF});
    }

}
