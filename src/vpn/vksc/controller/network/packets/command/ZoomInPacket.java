package vpn.vksc.controller.network.packets.command;

import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.packets.PayloadType;
import vpn.vksc.controller.network.packets.ViscaPacket;

public class ZoomInPacket extends ViscaPacket {
    public ZoomInPacket(int sequenceNumber) {
        super(PayloadType.VISCA_COMMAND, sequenceNumber,
                HexUtils.stringToByteArray("<0x81><0x01><0x04><0x07><0x02><0xFF>"));
    }

    public ZoomInPacket(int sequenceNumber, int zoomSpeed) {
        super(PayloadType.VISCA_COMMAND, sequenceNumber, new byte[0]);

        setPayload(HexUtils.stringToByteArray("<0x81><0x01><0x04><0x07><0x2"+ Integer.toHexString(zoomSpeed % 8) +"><0xFF>"));
    }
}
