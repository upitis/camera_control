package vpn.vksc.controller.network.packets.command;

import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.packets.PayloadType;
import vpn.vksc.controller.network.packets.ViscaPacket;

public class ZoomOutPacket extends ViscaPacket {
    public ZoomOutPacket(int sequenceNumber) {
        super(PayloadType.VISCA_COMMAND, sequenceNumber,
                HexUtils.stringToByteArray("<0x81><0x01><0x04><0x07><0x03><0xFF>"));
    }

    public ZoomOutPacket(int sequenceNumber, int zoomSpeed) {
        super(PayloadType.VISCA_COMMAND, sequenceNumber, new byte[0]);

        setPayload(HexUtils.stringToByteArray("<0x81><0x01><0x04><0x07><0x3"+ Integer.toHexString(zoomSpeed % 8) +"><0xFF>"));
    }

}
