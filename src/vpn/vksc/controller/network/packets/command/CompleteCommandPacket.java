package vpn.vksc.controller.network.packets.command;

import vpn.vksc.controller.network.packets.ViscaPacket;

public class CompleteCommandPacket extends ViscaPacket {
    public CompleteCommandPacket(byte[] rawData) {
        super(rawData);
    }

    @Override
    public String toString() {
        return "Command with sequence number <" + getSequenceNumber() + "> is completed by camera";
    }
}
