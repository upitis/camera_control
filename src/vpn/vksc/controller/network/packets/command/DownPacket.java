package vpn.vksc.controller.network.packets.command;

import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.packets.PayloadType;
import vpn.vksc.controller.network.packets.ViscaPacket;

public class DownPacket extends ViscaPacket {
    public DownPacket(int sequenceNumber, int panSpeed, int tiltSpeed) {
        super(PayloadType.VISCA_COMMAND, sequenceNumber, new byte[0]);

        String s1 = "<0x81><0x01><0x06><0x01>";
        // panSpeed 0x01 - 0x18 (hex) or 1 - 24 (dec)
        String pan = HexUtils.shortToMyHexString((byte)(panSpeed % 25));
        // tiltSpeed 0x01 - 0x17 (hex) or 1 - 23 (dec)
        String tilt = HexUtils.shortToMyHexString((byte)(tiltSpeed % 24));
        String s2 = "<0x03><0x02><0xFF>";
        this.setPayload(HexUtils.stringToByteArray(s1 + pan + tilt + s2));
    }
}
