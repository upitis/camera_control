package vpn.vksc.controller.network.packets;

import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class ViscaPacket {
    private PayloadType payloadType;
    private short payloadLength;
    private int sequenceNumber;
    private byte[] payload;
    private byte[] rawData;

    public ViscaPacket(PayloadType payloadType, int sequenceNumber, byte[] payload) {
        this.payload = payload;
        this.sequenceNumber = sequenceNumber;
        this.payloadType = payloadType;
        this.payloadLength = (short)payload.length;
        this.rawData = arrayConcat(this.payloadType.getBytes(),
                ByteBuffer.allocate(2).putShort(this.payloadLength).array(),
                ByteBuffer.allocate(4).putInt(this.sequenceNumber).array(), this.payload);
    }

    public ViscaPacket(byte[] rawData) {
//        this.rawData = rawData;
        byte[] plT = new byte[2];
        byte[] plL = new byte[2];
        byte[] sqN = new byte[4];
        System.arraycopy(rawData, 0, plT, 0, 2);
        System.arraycopy(rawData, 2, plL, 0, 2);
        System.arraycopy(rawData, 4, sqN, 0, 4);
        this.payloadLength = ByteBuffer.wrap(plL).getShort();
        this.sequenceNumber = ByteBuffer.wrap(sqN).getInt();
        this.payload = new byte[payloadLength];
        System.arraycopy(rawData, 8, this.payload, 0, payloadLength);

        for (PayloadType payloadType  : PayloadType.values()){
            if (payloadType.getShort() == ByteBuffer.wrap(plT).getShort()){
                this.payloadType = payloadType;
                break;
            }
        }


        this.rawData = arrayConcat(this.payloadType.getBytes(),
                ByteBuffer.allocate(2).putShort(this.payloadLength).array(),
                ByteBuffer.allocate(4).putInt(this.sequenceNumber).array(), this.payload);

    }

    protected static  byte[] arrayConcat(byte[] arr1, byte[] arr2, byte[] arr3, byte[] arr4){
        byte[] arr = new byte[arr1.length + arr2.length + arr3.length + arr4.length];
        System.arraycopy(arr1, 0, arr, 0, arr1.length);
        System.arraycopy(arr2, 0, arr, arr1.length, arr2.length);
        System.arraycopy(arr3, 0, arr, arr1.length + arr2.length, arr3.length );
        System.arraycopy(arr4, 0, arr, arr1.length + arr2.length + arr3.length, arr4.length );
        return arr;
    }

    public PayloadType getPayloadType() {
        return payloadType;
    }

    public short getPayloadLength() {
        return payloadLength;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public byte[] getPayload() {
        return payload;
    }

    public byte[] getRawData() {
        return rawData;
    }

    public void setPayload(byte[] payload) {
        this.payload = payload;
        this.payloadLength = (short) payload.length;
        this.rawData = arrayConcat(this.payloadType.getBytes(),
                ByteBuffer.allocate(2).putShort(this.payloadLength).array(),
                ByteBuffer.allocate(4).putInt(this.sequenceNumber).array(), payload);
        this.payload = payload;
    }


    @Override
    public String toString() {
        return new String(HexBin.encode(rawData));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;

        ViscaPacket packet = (ViscaPacket) o;

        return sequenceNumber == packet.sequenceNumber;
    }

    @Override
    public int hashCode() {
        return sequenceNumber;
    }
}
