package vpn.vksc.controller.network.packets;

public class ResetSequencePacket extends ViscaPacket {
    public ResetSequencePacket() {
        super(PayloadType.CONTROL_COMMAND, 0,
                new byte[] {(byte)0x01});
    }
}
