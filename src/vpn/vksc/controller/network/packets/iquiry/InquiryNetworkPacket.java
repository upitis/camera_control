package vpn.vksc.controller.network.packets.iquiry;

import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.packets.PayloadType;
import vpn.vksc.controller.network.packets.ViscaPacket;

public class InquiryNetworkPacket extends ViscaPacket {

    public InquiryNetworkPacket() {
        super(PayloadType.VISCA_INQUIRY,0,
                HexUtils.stringToByteArray("<0x02>ENQ:network<0xFF><0x03>"));

    }
}

