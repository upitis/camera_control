package vpn.vksc.controller.network.packets.iquiry;

import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.packets.PayloadType;
import vpn.vksc.controller.network.packets.ViscaPacket;

public class NetworkSettingPacket extends ViscaPacket {

    public NetworkSettingPacket(String mac, String ip, String mask, String name) {
        super(PayloadType.VISCA_DEVICE_SETTING_COMMAND, 0,
                HexUtils.stringToByteArray("<0x02>MAC:" + mac + "<0xFF>IPADR:" + ip + "<0xFF>MASK:" + mask +
                                            "<0xFF>NAME:" + name + "<0xFF><0x03>"));
    }

}
