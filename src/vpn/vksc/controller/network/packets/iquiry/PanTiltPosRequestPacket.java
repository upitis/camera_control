package vpn.vksc.controller.network.packets.iquiry;

import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.packets.PayloadType;
import vpn.vksc.controller.network.packets.ViscaPacket;

public class PanTiltPosRequestPacket extends ViscaPacket {
    public PanTiltPosRequestPacket(int sequenceNumber) {
        super(PayloadType.VISCA_COMMAND, sequenceNumber,
                HexUtils.stringToByteArray("<0x81><0x09><0x06><0x12><0xFF>"));
    }
}
