package vpn.vksc.controller.network.packets.iquiry;

import vpn.vksc.controller.network.packets.ViscaPacket;

public class PanTiltPosReplyPacket extends ViscaPacket {
    // 0xDE00-0x2200 (-24064 - 24064 (dec))
    private short panPos = 0;
    // 0xEE00-0x0400 (-24064 - 24064 (dec))
    private short tiltPos = 0;

    public PanTiltPosReplyPacket(byte[] rawData) {
        super(rawData);
        if (this.getPayloadLength() == 11 && this.getPayload()[0] == (byte)0x90 && this.getPayload()[1] == (byte)0x50) {
            panPos = (short) (((short)getPayload()[2] << 12) + ((short)getPayload()[3] << 8) +((short)getPayload()[4] << 4) + (short)getPayload()[5]);
            tiltPos = (short) (((short)getPayload()[6] << 12) + ((short)getPayload()[7] << 8) +((short)getPayload()[8] << 4) + (short)getPayload()[9]);
        }
    }


    public short getPanPos() {
        return panPos;
    }

    public short getTiltPos() {
        return tiltPos;
    }

    @Override
    public String toString() {
        return "Camera position: (pan=" + getPanPos() + "; tilt=" + getTiltPos()+ ")";
    }
}
