package vpn.vksc.controller.network.packets.iquiry;

import vpn.vksc.controller.network.packets.ViscaPacket;

public class ZoomPosReplyPacket extends ViscaPacket {
    private short zoom;

    public ZoomPosReplyPacket(byte[] rawData) {
        super(rawData);
        if (this.getPayloadLength() == 7 && this.getPayload()[0] == (byte)0x90 && this.getPayload()[1] == (byte)0x50) {
            zoom = (short) (((short)getPayload()[2] << 12) + ((short)getPayload()[3] << 8) +((short)getPayload()[4] << 4) + (short)getPayload()[5]);
        }
    }

    public short getZoom() {
        return zoom;
    }

    @Override
    public String toString() {
        return "Camera zoom position: " + getZoom();
    }
}
