package vpn.vksc.controller.network.packets.iquiry;

import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.packets.PayloadType;
import vpn.vksc.controller.network.packets.ViscaPacket;

public class ZoomPosRequestPacket extends ViscaPacket {
    public ZoomPosRequestPacket(int sequenceNumber) {
        super(PayloadType.VISCA_COMMAND, sequenceNumber,
                HexUtils.stringToByteArray("<0x81><0x09><0x04><0x47><0xFF>"));
    }
}
