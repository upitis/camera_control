package vpn.vksc.controller.network.packets.iquiry;

import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.packets.ViscaPacket;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InquiryNetworkReplyPacket extends ViscaPacket {
    private String macAddress = "";
    private String ipAddress = "";
    private String netmask = "";
    private String gateway = "";
    private String softVersion = "";
    private String name = "";

    public InquiryNetworkReplyPacket(byte[] rawData) {
        super(rawData);
        String s = this.toString().toUpperCase();
        Pattern pattern = Pattern.compile("02(.+)FF03");
        Matcher matcher = pattern.matcher(s);
        List<String> parameters = new ArrayList<>();
        if (matcher.find()) {
            Collections.addAll(parameters, matcher.group(1).split("FF"));
            for (String parameter : parameters) {
                String p = HexUtils.hexStringToAscii(parameter);
                if (p.contains("MAC:")) this.macAddress = p.split(":")[1];
                if (p.contains("SOFTVERSION:")) this.softVersion = p.split(":")[1];
                if (p.contains("IPADR:")) this.ipAddress = p.split(":")[1];
                if (p.contains("MASK:")) this.netmask = p.split(":")[1];
                if (p.contains("GATEWAY:")) this.gateway = p.split(":")[1];
                if (p.contains("NAME:")) this.name = p.split(":")[1];
            }
        }
    }

    public String getMacAddress() {
        return macAddress;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getNetmask() {
        return netmask;
    }

    public String getGateway() {
        return gateway;
    }

    public String getSoftVersion() {
        return softVersion;
    }

    public String getName() {
        return name;
    }
}
