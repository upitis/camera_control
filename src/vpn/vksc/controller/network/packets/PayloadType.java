package vpn.vksc.controller.network.packets;

import java.nio.ByteBuffer;

public enum PayloadType {
    VISCA_COMMAND(new byte[]{(byte)0x01,(byte)0x00}),
    VISCA_INQUIRY(new byte[]{(byte)0x01,(byte)0x10}),
    VISCA_REPLY(new byte[]{(byte)0x01,(byte)0x11}),
    VISCA_DEVICE_SETTING_COMMAND(new byte[]{(byte)0x01,(byte)0x20}),
    CONTROL_COMMAND(new byte[]{(byte)0x02,(byte)0x00}),
    CONTROL_REPLY(new byte[]{(byte)0x02,(byte)0x01}),

    NETWORK(new byte[]{(byte)0xCC,(byte)0x9C});

    byte[] bytes;

    PayloadType(byte[] bytes) {
        this.bytes = bytes;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public short getShort() {
        return  ByteBuffer.wrap(bytes,0,2).getShort();
    }
}
