package vpn.vksc.controller.network;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HexUtils {

    public static String hexStringToAscii(String hexString){
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < hexString.length(); i+=2) {
            String str = hexString.substring(i, i+2);
            output.append((char)Integer.parseInt(str, 16));
        }
        return output.toString();
    }

    public static byte[] stringToByteArray(String s){
        Pattern pattern = Pattern.compile("<0x[0-9a-fA-F]{2}>");
        Matcher matcher = pattern.matcher(s);
        while (matcher.find()) {
            s = s.replace(matcher.group(), hexStringToCharString(matcher.group()));
        }
        char[] chars = s.toCharArray();
        byte[] result = new byte[chars.length];
        for (int i = 0; i < chars.length; i++) {
            byte b = (byte) chars[i];
            result[i] = b;
        }
        return result;
    }

    // hex String like this: 0xAF
    private static String hexStringToCharString(String s) {
        String res = s;
        if (s.matches("<0x[0-9a-fA-F]{2}>")) {
            int data = ((Character.digit(s.charAt(3), 16) << 4)
                    + Character.digit(s.charAt(4), 16));
            char ch = Character.toChars(data)[0];

            res = String.valueOf(ch);
        }
        return res;
    }

    // MyHexString is in format <0xXX>
    public static String shortToMyHexString(byte number) {
        String num = Integer.toHexString(number);
        if (num.length() == 1) {
            num = "0" + num;
        }
        return  "<0x" + num +  ">";
    }

}
