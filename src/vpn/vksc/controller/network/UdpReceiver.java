package vpn.vksc.controller.network;

import vpn.vksc.controller.network.packets.PayloadType;
import vpn.vksc.controller.network.packets.ViscaPacket;
import vpn.vksc.controller.network.packets.command.AckPacket;
import vpn.vksc.controller.network.packets.command.CompleteCommandPacket;
import vpn.vksc.controller.network.packets.iquiry.InquiryNetworkReplyPacket;
import vpn.vksc.controller.network.packets.iquiry.PanTiltPosReplyPacket;
import vpn.vksc.controller.network.packets.iquiry.ZoomPosReplyPacket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class UdpReceiver {
    private int port;
    private DatagramSocket datagramSocket;
    private byte[] buf;

    public UdpReceiver(int port) {
        this.port = port;
        try {
            this.datagramSocket = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public ViscaPacket receivePacket(Boolean isWithHeader){
        buf = new byte[1024];
        DatagramPacket dp = new DatagramPacket(buf,1024);
        try {
            datagramSocket.receive(dp);
            ViscaPacket packet;
            if (isWithHeader) {
                packet = new ViscaPacket(dp.getData());
            }else {
                PayloadType payloadType = PayloadType.VISCA_REPLY;
                if (dp.getPort() == 52380) payloadType = PayloadType.NETWORK;
                packet = new ViscaPacket(payloadType,0,buf);
            }
            return packetClassifier(packet);
        } catch (SocketTimeoutException ex) {

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private ViscaPacket packetClassifier(ViscaPacket packet) {
        // Классификатор пакетов

        ViscaPacket classifiedPacket = packet;
        //PanTiltPosReplyPacket
        if (packet.getPayloadType() == PayloadType.VISCA_REPLY && packet.getPayloadLength() == 11 &&
                packet.getPayload()[0] == (byte)0x90 && packet.getPayload()[1] == (byte)0x50) {
            classifiedPacket = new PanTiltPosReplyPacket(packet.getRawData());
        }
        //ZoomPosReplyPacket
        if (packet.getPayloadType() == PayloadType.VISCA_REPLY && packet.getPayloadLength() == 7 &&
                packet.getPayload()[0] == (byte)0x90 && packet.getPayload()[1] == (byte)0x50) {
            classifiedPacket = new ZoomPosReplyPacket(packet.getRawData());
        }
        //AckPacket
        if (packet.getPayloadType() == PayloadType.VISCA_REPLY &&  packet.getPayloadLength() == 3  &&
                packet.getPayload()[0] == (byte)0x90 && packet.getPayload()[1] == (byte)0x41) {
            classifiedPacket = new AckPacket(packet.getRawData());
        }
        //CompletionPacket
        if (packet.getPayloadType() == PayloadType.VISCA_REPLY &&  packet.getPayloadLength() == 3  &&
                packet.getPayload()[0] == (byte)0x90 && packet.getPayload()[1] == (byte)0x51) {
            classifiedPacket = new CompleteCommandPacket(packet.getRawData());
        }
        //ErrorPacket
        if (packet.getPayloadType() == PayloadType.VISCA_REPLY &&  packet.getPayloadLength() == 3  &&
                packet.getPayload()[0] == (byte)0x90 &&
                (packet.getPayload()[1] == (byte)0x60 || packet.getPayload()[1] == (byte)0x61)) {
            classifiedPacket = new AckPacket(packet.getRawData());
        }
        //NetworkPacker
        if (packet.getPayloadType() == PayloadType.NETWORK &&  packet.getPayloadLength() > 50  &&
                packet.getPayload()[0] == (byte)0x02) {
            classifiedPacket = new InquiryNetworkReplyPacket(packet.getRawData());
        }

        return classifiedPacket;

    }

    public DatagramSocket getDatagramSocket() {
        return datagramSocket;
    }


}
