package vpn.vksc.controller;

import vpn.vksc.controller.network.HexUtils;
import vpn.vksc.controller.network.UdpReceiver;
import vpn.vksc.controller.network.UdpSender;
import vpn.vksc.controller.network.packets.*;
import vpn.vksc.controller.network.packets.command.*;
import vpn.vksc.controller.network.packets.iquiry.*;
import vpn.vksc.model.Camera;

import java.net.SocketException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CameraController {
    private final static int COMMAND_PORT = 52381;
    private final static int COMMAND_NETWORK_PORT = 52380;
    private final static String BROADCAST_IPADDRESS = "255.255.255.255";

    private static CameraController ourInstance = new CameraController();
    private Camera activeCam = new Camera("192.168.0.100");
    private UdpSender udpSender = null;
    private UdpReceiver udpReceiver = new UdpReceiver(COMMAND_PORT);
    private UdpSender networkConfigSender = new UdpSender(BROADCAST_IPADDRESS, COMMAND_NETWORK_PORT);
    private UdpReceiver networkConfigReceiver;

    private BlockingQueue<ViscaPacket> outBuffer = new LinkedBlockingQueue<>();
    private BlockingQueue<ViscaPacket> inBuffer = new LinkedBlockingQueue<>();

    private ExecutorService executorSendingService = Executors.newSingleThreadExecutor();
    private ExecutorService executorReceivingService = Executors.newSingleThreadExecutor();
    private ExecutorService executorCompletionService = Executors.newSingleThreadExecutor();
    private ExecutorService executorFindingNetworkService = Executors.newSingleThreadExecutor();
    private Future<List<ViscaPacket>> viscaCamerasInNetwork;




    public static CameraController getInstance() {
        return ourInstance;
    }

    private CameraController() {

        // Looking in outBuffer for Sending commands
        executorSendingService.execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
//                    ViscaPacket packet = outBuffer.peek(); //wait for remove from queue by another service
                    ViscaPacket packet = outBuffer.poll(); //send one time and remove from queue
                    if (packet != null && udpSender != null) {
                        if (udpSender.sendData(packet, true)) {
                            System.out.println("Sending: \"" + packet + "\" ... Sequence number=<" + packet.getSequenceNumber() + ">");
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });

        // Receiving UDP packets and put them to inBuffer
        executorReceivingService.execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    ViscaPacket receivedPacket =udpReceiver.receivePacket(true);
                    if (receivedPacket != null) {
                        try {
                            inBuffer.put(receivedPacket);
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        //Polling inBuffer. If command was acknowledged when remove this command from outBuffer
        executorCompletionService.execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    ViscaPacket inPacket = inBuffer.poll();
                    if (inPacket != null) {
                        outBuffer.remove(inPacket); // equals by sequence number, so remove the sending packet with the same sn
                        if (inPacket instanceof CompleteCommandPacket) {
                            try {
                                Thread.sleep(500);
                                outBuffer.put(new PanTiltPosRequestPacket(activeCam.increaseSequenceNumber()));
                                outBuffer.put(new ZoomPosRequestPacket(activeCam.increaseSequenceNumber()));
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if (inPacket instanceof ErrorPacket) {
                            outBuffer.remove(inPacket);
                        }
                        if (inPacket instanceof PanTiltPosReplyPacket) {
                            activeCam.setPanPos(((PanTiltPosReplyPacket) inPacket).getPanPos());
                            activeCam.setTiltPos(((PanTiltPosReplyPacket) inPacket).getTiltPos());
                        }
                        if (inPacket instanceof ZoomPosReplyPacket) {
                            activeCam.setZoom(((ZoomPosReplyPacket) inPacket).getZoom());
                        }
                        System.out.println(inPacket);
                    }
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    public void recallPreset(int preset) {
        ViscaPacket presetPacket = new PresetPacket((byte) preset, activeCam.increaseSequenceNumber());
        try {
            outBuffer.put(presetPacket);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void savePreset(int preset) {
        ViscaPacket packet = new SavePresetPacket(activeCam.increaseSequenceNumber(), (byte) preset);
        try {
            outBuffer.put(packet);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setAbsPos(short pan, short tilt) {
        ViscaPacket packet = new AbsolutePositionPacket(activeCam.increaseSequenceNumber(), 0x18, 0x17, pan, tilt);
        try {
            outBuffer.put(packet);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setZoomPos(short zoomPos) {
        ViscaPacket packet = new ZoomPosPacket(activeCam.increaseSequenceNumber(), zoomPos);
        try {
            outBuffer.put(packet);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void changeCameraIpAddress(String mac, String ip, String mask, String name) {
        ViscaPacket networkPacket = new NetworkSettingPacket(mac, ip, mask, name);
        networkConfigSender.sendData(networkPacket, false);
        System.out.println(networkPacket);
    }

    private void startServiceOfLookingCamerasInBroadcastDomain(){
        ViscaPacket inquiryNetwork = new InquiryNetworkPacket();
        networkConfigSender.sendData(inquiryNetwork,false);
        networkConfigReceiver= new UdpReceiver(networkConfigSender.getUsedLocalPort());
        try {
            networkConfigReceiver.getDatagramSocket().setBroadcast(true);
            networkConfigReceiver.getDatagramSocket().setSoTimeout(1000);
        } catch (SocketException e) {
            e.printStackTrace();
        }

        viscaCamerasInNetwork = executorFindingNetworkService.submit(new Callable<List<ViscaPacket>>() {
            @Override
            public List<ViscaPacket> call() throws Exception {
                List<ViscaPacket> networkConfigList = new ArrayList<>();
                ViscaPacket packet;
                Instant previous, current;
                previous = Instant.now();
                do {
                    packet = networkConfigReceiver.receivePacket(false);
                    if (packet != null) {
                        networkConfigList.add(packet);
                    }
                    current = Instant.now();
                } while (ChronoUnit.SECONDS.between(previous,current) < 2);
                return networkConfigList;
            }
        });
    }

    public List<Camera> findSonyCameraInBroadcastDomain(){
        List<Camera> camList = new ArrayList<>();

        startServiceOfLookingCamerasInBroadcastDomain();

        try {
            for (ViscaPacket networkPacketReply: viscaCamerasInNetwork.get()) {

                if (networkPacketReply instanceof InquiryNetworkReplyPacket) {
                    Camera sonyCam = new Camera();

                    sonyCam.setMacAddress(((InquiryNetworkReplyPacket) networkPacketReply).getMacAddress());
                    sonyCam.setSoftVersion(((InquiryNetworkReplyPacket) networkPacketReply).getSoftVersion());
                    sonyCam.setIpAddress(((InquiryNetworkReplyPacket) networkPacketReply).getIpAddress());
                    sonyCam.setNetmask(((InquiryNetworkReplyPacket) networkPacketReply).getNetmask());
                    sonyCam.setGateway(((InquiryNetworkReplyPacket) networkPacketReply).getGateway());
                    sonyCam.setName(((InquiryNetworkReplyPacket) networkPacketReply).getName());

                    camList.add(sonyCam);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return camList;
    }

    public void setIpAddress(String ipAddress) {
        if (ipAddress.matches("\\d{1,3}[.]\\d{1,3}[.]\\d{1,3}[.]\\d{1,3}")) {
            outBuffer.clear();
            inBuffer.clear();
            activeCam = new Camera(ipAddress);
            udpSender = new UdpSender(ipAddress, COMMAND_PORT);
            udpSender.sendData(new ResetSequencePacket(), true);

        }
    }

    public void up(int speed, int delayInMillis){
        try {
            outBuffer.put(new UpPacket(activeCam.increaseSequenceNumber(), speed, speed));
            Thread.sleep(delayInMillis);
            outBuffer.put(new StopPacket(activeCam.increaseSequenceNumber(), 1, 1));
        } catch (InterruptedException e) { e.printStackTrace();  }
    }

    public void down(int speed, int delayInMillis){
        try {
            outBuffer.put(new DownPacket(activeCam.increaseSequenceNumber(), speed, speed));
            Thread.sleep(delayInMillis);
            outBuffer.put(new StopPacket(activeCam.increaseSequenceNumber(), 1, 1));
        } catch (InterruptedException e) { e.printStackTrace();  }

    }

    public void left(int speed, int delayInMillis){
        try {
            outBuffer.put(new LeftPacket(activeCam.increaseSequenceNumber(), speed, speed));
            Thread.sleep(delayInMillis);
            outBuffer.put(new StopPacket(activeCam.increaseSequenceNumber(), 1, 1));
        } catch (InterruptedException e) { e.printStackTrace();  }
    }

    public void right(int speed, int delayInMillis) {
        try {
            outBuffer.put(new RightPacket(activeCam.increaseSequenceNumber(), speed, speed));
            Thread.sleep(delayInMillis);
            outBuffer.put(new StopPacket(activeCam.increaseSequenceNumber(), 1, 1));
        } catch (InterruptedException e) { e.printStackTrace();  }
    }

    public void osdOn(){
        try {
            outBuffer.put(new InformationDisplayOnPacket(activeCam.increaseSequenceNumber()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void osdOff() {
        try {
            outBuffer.put(new InformationDisplayOffPacket(activeCam.increaseSequenceNumber()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void zoomIn(int zoomSpeed, int delayInMilis){
        try {
            outBuffer.put(new ZoomInPacket(activeCam.increaseSequenceNumber(), zoomSpeed));
            Thread.sleep(delayInMilis);
            outBuffer.put(new ZoomStopPacket(activeCam.increaseSequenceNumber()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void zoomOut(int zoomSpeed, int delayInMillis) {
        try {
            outBuffer.put(new ZoomOutPacket(activeCam.increaseSequenceNumber(),zoomSpeed));
            Thread.sleep(delayInMillis);
            outBuffer.put(new ZoomStopPacket(activeCam.increaseSequenceNumber()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void powerOn() {
        try {
            outBuffer.put(new PowerOnPacket(activeCam.increaseSequenceNumber()));
            activeCam.setPowered(true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void powerOff() {
        try {
            outBuffer.put(new PowerOffPacket(activeCam.increaseSequenceNumber()));
            activeCam.setPowered(false);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Future<List<ViscaPacket>> getViscaCamerasInNetwork() {
        return viscaCamerasInNetwork;
    }
}
