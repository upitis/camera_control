package vpn.vksc.model;

public class Camera{
    private String macAddress;
    private String ipAddress = "192.168.0.100"; //default for Sony
    private String netmask;
    private String gateway;
    private String name;
    private String softVersion;
    private int sequenceNumber = 0;
    private short panPos; //0xDE00 - 0x2200
    private short tiltPos; //0xFC00 - 0x1200
    private short zoom; //pqrq (0 - 31424 (dec))
    private boolean isPowered = true;

    public Camera(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Camera() {
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getNetmask() {
        return netmask;
    }

    public void setNetmask(String netmask) {
        this.netmask = netmask;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoftVersion() {
        return softVersion;
    }

    public void setSoftVersion(String softVersion) {
        this.softVersion = softVersion;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public int increaseSequenceNumber() {
        this.sequenceNumber++;
        return sequenceNumber;
    }

    public short getPanPos() {
        return panPos;
    }

    public void setPanPos(short panPos) {
        this.panPos = panPos;
    }

    public short getTiltPos() {
        return tiltPos;
    }

    public void setTiltPos(short tiltPos) {
        this.tiltPos = tiltPos;
    }

    public short getZoom() {
        return zoom;
    }

    public void setZoom(short zoom) {
        this.zoom = zoom;
    }

    public boolean isPowered() {
        return isPowered;
    }

    public void setPowered(boolean powered) {
        isPowered = powered;
    }

    @Override
    public String toString() {
        return "Camera{" +
                "macAddress='" + macAddress + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", netmask='" + netmask + '\'' +
                ", gateway='" + gateway + '\'' +
                ", name='" + name + '\'' +
                ", softVersion='" + softVersion + '\'' +
                '}';
    }
}
